
/**
 * @NApiVersion 2.x
 * @NSScriptVersion 1.0
 * 
 * @file LIB_script_status.js
 * @name LIB_script_status.js
 * @description Use this script as a listner for script @governance. This library is compatible with 1.0 and 2.0 NS scripts versions.
 * @copyright MIT
 * @author fullstack.studio
 * @contributors 
 * @since 11 September 2019
 * @version 1.0.1
 */
'use strict';
/**
 * 
 * @description use to init the script ScriptStatus[V1]. 1st step. main function.
 * @example <caption>How to set the script listner:</caption>
 * var script = setScriptListener({
 *          stopValue : 200 //| mandatory
 *          recallValue: 200 //| mandatory
 *          canBeRecalled: boolean //| mandatory
 *          version: 1.0 //or  2.0  //| optional
 * });
 * See: .canContinue() from {@link ScriptStatus} or {@link ScriptStatusV1}
 * @returns {object} @object ScriptStatus[V1]
 */
function setScriptListener() {
    var param = {};
    if (arguments.length === 1) {
        param = arguments[0];
    } else {
        if (arguments.length > 2) {
            param.stopValue = arguments[0];
            param.recallValue = arguments[1];
            param.canBeRecalled = arguments[2];
            param.version = arguments[3];
        }
    }
    param.version = param.version || (typeof nlapiGetContext === 'function' ? '1.0' : '2.0');
    param.version += ".0";
    var script = null;
    if (param.version.indexOf('1.') === 0) {
        script = new ScriptStatusV1(param.stopValue, param.recallValue, param.canBeRecalled);
    } else if ((param.version).indexOf('2.') === 0) {
        script = new ScriptStatus(param.stopValue, param.recallValue, param.canBeRecalled);
    } else {
        throw "Not supported script version";
    }
    return script.init();
}

/**
 * @class
 * @classdesc Script status listner. this support only 2.0 scripts.
 * @param {*} _stopValue 
 * @param {*} _recallValue 
 * @param {*} _canBeRecalled 
 */
function ScriptStatus(_stopValue, _recallValue, _canBeRecalled) {
    this._ = {};
    this.script = '';
    this.scriptId = '';
    this.deploymentId = '';
    this.stopValue = _stopValue || 0;
    this.recallValue = _recallValue || 0;
    this.canBeRecalled = _canBeRecalled || false;
    this.stopExecution = false;
    this.parameters = {};
    this.recall = function () {
        // log.debug('ScriptStatus recall', 'begin');
        var ss_task = this._.task.create({
            taskType: this._.task.TaskType.SCHEDULED_SCRIPT
        });
        ss_task.scriptId = this.scriptId;
        ss_task.deploymentId = this.deploymentId;
        var ss_task_id = ss_task.submit();
        // log.debug('ScriptStatus recall', 'end : ss_task_id :' + ss_task_id);
    };
    this.checkStatus = function () {
        var remainingUsage = this.script.getRemainingUsage();
        // log.debug('ScriptStatus getRemainingUsage', remainingUsage);
        if (remainingUsage <= this.stopValue) {
            if (this.canBeRecalled) {
                this.recall();
            }
            this.stopExecution = true;
        }
    };
    this.init = function () {
        var this_ = this;
        require(['N/task', 'N/runtime'], function (task, runtime) {
            this_._.task = task;
            this_._.runtime = runtime;
            this_.script = runtime.getCurrentScript();
            this_.deploymentId = this_.script.deploymentId;
            this_.scriptId = this_.script.id;
        });
        Object.preventExtensions(this);
        return this;
    };

    /**
     * @example <caption>How to control the script</caption>
     * if(script.canContinue()){
     *  //your code
     * }
     * @description call this function each time before "next" step. use this function to control the script.
     * @returns {boolean}
     */
    this.canContinue = function () {
        this.checkStatus();
        return !this.stopExecution;
    };
    this.getRemainingUsage = function () {
        return this.script.getRemainingUsage();
    };
    this.setValue = function (_prop, _val) {
        if (this.hasOwnProperty(_prop)) {
            this[_prop] = _val;
        }
    };
    this.getValue = function (_prop) {
        var val = null;
        if (this.hasOwnProperty(_prop)) {
            val = this[_prop];
        }
        return val;
    };
    /** runtime script functions  #ideas */
    this.getParameters = function () {
        var this_ = this;
        if (arguments.length > 0) {
            var params_keys = [];
            if (Array.isArray(arguments[0])) {
                params_keys = arguments[0]
            } else {
                for (var arg = 0; arg < arguments.length; arg++) {
                    params_keys.push(arguments[arg]);
                }
            }
            for (var k = 0; k < params_keys.length; k++) {
                var key = params_keys[k];
                try {
                    this_.parameters[key] = this_.getParameter(key);
                } catch (e) {
                    log.error(key, "this param doesn't exist")
                }
            }
        }
        return this.parameters;
    };
    this.getParameter = function () {
        var key = arguments[0];
        return this.script.getParameter({ name: key });
    }
    /** /runtime script functions  */
}

/**
 * @class
 * @classdesc Script status listner. this support only 1.0 scripts
 * @param {*} _stopValue
 * @param {*} _recallValue
 * @param {*} _canBeRecalled
 */
function ScriptStatusV1(_stopValue, _recallValue, _canBeRecalled) {
    this._ = {};
    this.script = '';
    this.scriptId = '';
    this.deploymentId = '';
    this.stopValue = _stopValue || 0;
    this.recallValue = _recallValue || 0;
    this.canBeRecalled = _canBeRecalled || false;
    this.stopExecution = false;
    this.recall = function () {
        // nlapiLogExecution('DEBUG', 'ScriptStatusV1 recall', 'begin');
        var status = nlapiScheduleScript(this.scriptId, this.deploymentId);
        // nlapiLogExecution('DEBUG', 'ScriptStatusV1 recall', 'end : status :' + status);
    };
    this.checkStatus = function () {
        var remainingUsage = this.script.getRemainingUsage();
        // nlapiLogExecution('DEBUG', 'ScriptStatusV1 getRemainingUsage', remainingUsage);
        if (remainingUsage <= this.stopValue) {
            if (this.canBeRecalled) {
                this.recall();
            }
            this.stopExecution = true;
        }
    };
    this.init = function () {
        this.script = nlapiGetContext();
        this.deploymentId = this.script.getDeploymentId();
        this.scriptId = this.script.getScriptId();
        Object.preventExtensions(this);
        return this;
    };
    /**
     * @example <caption>How to control the script</caption>
     * if(script.canContinue()){
     *  //your code
     * }
     * @description call this function each time before "next" step. use this function to control the script.
     * @returns {boolean}
     */
    this.canContinue = function () {
        this.checkStatus();
        return !this.stopExecution;
    };
    this.getRemainingUsage = function () {
        return this.script.getRemainingUsage();
    };
    this.setValue = function (_prop, _val) {
        if (this.hasOwnProperty(_prop)) {
            this[_prop] = _val;
        }
    };
    this.getValue = function (_prop) {
        var val = null;
        if (this.hasOwnProperty(_prop)) {
            val = this[_prop];
        }
        return val;
    };
}
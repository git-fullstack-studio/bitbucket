/**
 *
 * @file helpers/isEmpty.js
 * @name isEmpty.js 
 * 
 * @company fullstack.studio
 * @copyright MIT
 * @author fullstack.studio
 * @contributors 
 * @since 11 September 2019
 * @version 1.0.1
 */
/**
 * @description verify if data contains any values diferent of spaces, null or undefined.
 * @param {*} data
 * @returns {boolean}
 */
function isEmpty(data) {
    if (typeof (data) === 'undefined' ||
        data === null) {
        return true;
    }
    if (typeof (data) === 'number' ||
        typeof (data) === 'boolean' ||
        typeof (data) === 'function') {
        return false;
    }
    if (typeof (data) === 'string') {
        return data.trim().length === 0;
    }
    var count = 0;
    for (var k in data) {
        if (!isEmpty(data[k])) {
            count++;
        }
    }
    return count === 0;
}